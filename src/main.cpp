#include <Arduino.h>
#include <Wire.h>
#include "LowPower.h"
#include <Time.h>
#include <DS3232RTC.h>
#include <EEPROM.h>
#define SERIAL_DEBUG

#if defined(SERIAL_DEBUG) || defined(SERIAL_POS)
#define SERIAL_BPS 9600
#endif

// https://en.wikipedia.org/wiki/Sidereal_time
#define solar_ms_seconds_per_sidereal_day 86164091ull
// Offset at Longitude 0 on 2019-01-01 00:00:00
// use http://neoprogrammics.com/sidereal_time_calculator/index.php
// to calculate sidereal angle at Greenwich, which is 6.6907020497
// (decimal hours). Divide by 24 and multiply by
// solar_ms_seconds_per_sidereal_day, 24020761
// now subtract the ms between 2019-01-01 and 1970-01-01: 1546300800000
#define siderial_ms_offset 1546276779239ull

#define motor_pin 2       // motor driver pin (plus next 3)
#define step_delay_fast 3 // normaly used, small to save battery life

#define ALRM0 0x08 // Alarm register 0
#define ALRM1 0x0b // Alarm register 1

uint8_t mhd[3] = {0, 0, 0}; // minute-hour-day
double loc_lng_d;
int rtc_power_pin = 8;

/*
 * for a DS3232 or DS3231, replace the include above with
 * #include DS3232RTC.h
 * and add  * https://github.com/JChristensen/DS3232RTC to your personal
 * libraries
 */

int getInt(String msg)
{
  int retval;
  Serial.print(msg);
  retval = Serial.parseInt();
  Serial.print(retval);
  Serial.println();
  return retval;
}

double getDouble(String msg)
{
  double retval;
  Serial.print(msg);
  retval = Serial.parseFloat();
  Serial.print(retval);
  Serial.println();
  return retval;
}

double getSidereal(time_t t, double loc_lng_d)
{
  // t is a 32 bit value in solar seconds since midnight 01-01-1970.
  // loc_lng_d is the longitude in degrees, E is positive

  uint64_t ms;
  uint32_t ln;
  double tmp;

  ms = t * 1000ull;         // in ms units and 64 bits
  ms -= siderial_ms_offset; // see defines

  // longitude correction
  ln = loc_lng_d * solar_ms_seconds_per_sidereal_day / 360.0;
  ms += ln;

  // Calculation introduces 1 ms cummulative drift per day, so 0.36 seconds
  // per year. As one step corresponds to roughly 40 seconds, drift by this
  // calculation is totally neglectable over the course of several decades
  ms = ms % solar_ms_seconds_per_sidereal_day; // ms in the sideral day

  // convert ms to hours
  tmp = ms;
  tmp = tmp * 24.0 / solar_ms_seconds_per_sidereal_day;
  // Serial.println (tmp);
  return (tmp);
}

/*
double getSidereal(time_t t, double loc_lng_d)
{
  // date number since 2000-01-01 00:00:00
  // this is a more long term solution, but double precision
  // on arduino has not enough precision.
  double dj2000;
  dj2000 = t - 946684800.0; // seconds since 
  dj2000 /= 86400.0; // one full day = 1
  Serial.print (' ');
  Serial.print (dj2000 * 86400);
  // https://www.aa.quae.nl/en/reken/sterrentijd.html
  double th = 99.967794687 +
    360.98564736628603 * dj2000 +
    2.907879E-13 * dj2000 * dj2000 - 
    5.302E-22  * dj2000 * dj2000  * dj2000 +
    loc_lng_d;

  th /= 360.0; // once full circle = 1
  th -= floor(th); // reduce to 0-1 range
  th *= 24.0; // expand to hours
  return th;
}
*/

void rtcWrite(int adr, int value)
{
  uint16_t uvalue;
  // slight negative allowed for slop processing
  uvalue = value + 2000;
  mhd[0] = uvalue % 60;              // min
  mhd[1] = (uvalue / 60) % 24;       // hour
  mhd[2] = 1 + (uvalue / (60 * 24)); // day in month
  // write to alarm registers 1 or 2
  RTC.writeRTC(adr == 0 ? ALRM0 : ALRM1, mhd, 3);
}

void setUTC()
{
  int yy, mm, dd, hh, nn, ss;

  Serial.println(F("Enter the date and time in UTC"));
  yy = getInt(F("Year:"));
  mm = getInt(F("Month:"));
  dd = getInt(F("Day:"));
  hh = getInt(F("Hour:"));
  nn = getInt(F("Minutes:"));
  ss = getInt(F("Seconds:"));

  if (getInt("type '1' to set the time:") == 1)
  {
    // *************************************************************************
    // * vvv REMEMBER TO ALWAYS SET THE RTC in UTC time, NOT local time!!      *
    // *************************************************************************
    pinMode(rtc_power_pin, OUTPUT);
    digitalWrite(rtc_power_pin, HIGH); // power up RTC NVRAM chip
    delay(1);
    setTime(hh, nn, ss, dd, mm, yy); // set time object
    time_t n = now();
    RTC.set(n); // transfer to RTC
    rtcWrite(0, 0);
    rtcWrite(1, 0);
    digitalWrite(rtc_power_pin, LOW); // power down RTC NVRAM chip
    pinMode(rtc_power_pin, INPUT);
    Serial.print("Seconds since 01-01-1970:");
    Serial.println(n);
  }
}

void setLng()
{
  loc_lng_d = getDouble(F("Enter your longitude, postive is east of Greenwich, decimal:"));
}

void setRtc()
{
  pinMode(rtc_power_pin, INPUT);
  rtc_power_pin = getInt(F("Pin that powers the RTC (usually 8 or 11)"));
  if (rtc_power_pin == 0)
    rtc_power_pin = 8;
  pinMode(rtc_power_pin, OUTPUT);
}

#define ADMUX_VCCWRT1V1 (_BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1))
void showVcc()
{
  // set reference to VCC and the measurement to the internal 1.1V reference
  if (ADMUX != ADMUX_VCCWRT1V1)
  {
    ADMUX = ADMUX_VCCWRT1V1;
    delayMicroseconds(350); // wait for Vref to settle
  }
  ADCSRA |= _BV(ADSC); // start conversion
  while (bit_is_set(ADCSRA, ADSC))
  {
  }; // wait for it to finish
  Serial.println(1098.0 / ADC);
}

#define beep_pin 7 // Piezo beeper
void beeptest()
{
  pinMode(beep_pin, OUTPUT);
  digitalWrite(beep_pin, HIGH);
  delay(500);
  digitalWrite(beep_pin, LOW);
  pinMode(beep_pin, INPUT);
}

void setAgeRegister()
{
  pinMode(rtc_power_pin, OUTPUT);
  digitalWrite(rtc_power_pin, HIGH); // power up RTC NVRAM chip
  delay(1);
  int offset = RTC.readRTC(0x10);
  if (offset > 127)
    offset -= 256;
  Serial.print(F("Aging register is now:"));
  Serial.println(offset);

  offset = getInt(F("Enter aging, -128 - 127, higher means slower clock:"));
  if (offset < 0)
    offset += 256;

  if (offset >= 0 && offset <= 255 && getInt("type '1' to set the aging register:") == 1)
  {
    RTC.writeRTC(0x10, offset);
    EEPROM.write(3, offset);
  }
  digitalWrite(rtc_power_pin, LOW); // power down RTC NVRAM chip
  pinMode(rtc_power_pin, INPUT);
}

void power_on()
{
  delay(1); // let hardware stabilize
#ifdef SERIAL_BPS
  Serial.begin(SERIAL_BPS); // reinitialize serial
#endif
  digitalWrite(A4, HIGH);                   // enable pull-up resistors
  digitalWrite(A5, HIGH);                   // from the I2C pins
  TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWEA); // re-initialize twowire.
  digitalWrite(rtc_power_pin, HIGH);        // power up RTC NVRAM chip
  delay(1);                                 // let hardware stabilize
}

void power_off()
{
  delay(1);                         // let hardware stabilize
  TWCR = 0;                         // disable twowire
  digitalWrite(rtc_power_pin, LOW); // power down RTC NVRAM chip
  digitalWrite(A4, LOW);            // disable pull-up resistors
  digitalWrite(A5, LOW);            // from the I2C pins
#ifdef SERIAL_BPS
  Serial.end(); // close Serial
#endif
  delay(1); // let hardware stabilize
}

void sleep()
{
  Serial.println(F("Sleep..."));
  delay(100);
  power_off();
  LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
}

// stepper motor stuff *********************************************************
void step_energize(int motor_pin_first, int mode)
{
  mode ^= 0x03; // reverse

  // energize in one of 4 step positions, or remove power
  if (mode <= 3)
  {
    pinMode(motor_pin_first, OUTPUT);
    pinMode(motor_pin_first + 1, OUTPUT);
    pinMode(motor_pin_first + 2, OUTPUT);
    pinMode(motor_pin_first + 3, OUTPUT);
  }

  switch (mode)
  {
  case 0:
    digitalWrite(motor_pin_first, HIGH);
    digitalWrite(motor_pin_first + 1, HIGH);
    digitalWrite(motor_pin_first + 2, LOW);
    digitalWrite(motor_pin_first + 3, LOW);
    break;
  case 1:
    digitalWrite(motor_pin_first, HIGH);
    digitalWrite(motor_pin_first + 1, LOW);
    digitalWrite(motor_pin_first + 2, LOW);
    digitalWrite(motor_pin_first + 3, HIGH);
    break;
  case 2:
    digitalWrite(motor_pin_first, LOW);
    digitalWrite(motor_pin_first + 1, LOW);
    digitalWrite(motor_pin_first + 2, HIGH);
    digitalWrite(motor_pin_first + 3, HIGH);
    break;
  case 3:
    digitalWrite(motor_pin_first, LOW);
    digitalWrite(motor_pin_first + 1, HIGH);
    digitalWrite(motor_pin_first + 2, HIGH);
    digitalWrite(motor_pin_first + 3, LOW);
    break;
  default: // de-energize
    digitalWrite(motor_pin_first, LOW);
    digitalWrite(motor_pin_first + 1, LOW);
    digitalWrite(motor_pin_first + 2, LOW);
    digitalWrite(motor_pin_first + 3, LOW);
    pinMode(motor_pin_first, INPUT);
    pinMode(motor_pin_first + 1, INPUT);
    pinMode(motor_pin_first + 2, INPUT);
    pinMode(motor_pin_first + 3, INPUT);
    break;
  }
}

void rotate()
{
  unsigned long time_ms = millis() + 1;
  unsigned long step_position = 0;

//   Serial.println(F("If the motor is geared 2048 steps per rev it should take about 6 seconds for"));
//   Serial.println(F("one revolution and return to it's starting positon. For 2038 steps per rev it"));
//   Serial.println(F("will stop about a quarter rev overrotated. A 513 Steps per rev motor will"));
//   Serial.println(F("complete a rotation in 1.5 seconds and stop about half rev overrotated. This"));
//   Serial.println(F("test will take about 5 minutes and the motor should rotate clockwise when"));
//   Serial.println(F("looking down on it."));

  while ((long)(millis() < time_ms) > 0)
  {
  };
  // and energize at position 0
  step_energize(motor_pin, 0);
  time_ms += step_delay_fast;

  // the motor is energized here, but delta is non zero so the stepper loop
  // will be entered and it will be de-energized at the end
  while (step_position < 102400u)
  {
    step_energize(motor_pin, (int)(++step_position & 0x3));
    // wait until
    time_ms += step_position < 8 ? 500 : step_delay_fast;
    //time_ms += 2000;
    while ((long)(millis() < time_ms) > 0)
    {
    };
  }
  // de-energize motor
  step_energize(motor_pin, 99);
}

void print2d(int v)
{
  if (v < 10)
    Serial.print('0');
  Serial.print(v);
}

void setup()
{
  int choice;

  power_on(); // also initilizes Serial
  Serial.setTimeout(32000);
  delay(2000);

  while (true)
  {
    Serial.println();
    Serial.println(F("0: sleep"));
    Serial.println(F("1: set longitude, needed to display sidereal time"));
    Serial.println(F("2: set RTC power pin"));
    Serial.println(F("3: set UTC and save to real time clock"));
    Serial.println(F("4: Set RTC aging register"));
    Serial.println(F("5: check motor gearing (takes 5 minutes)"));
    Serial.println(F("6: battery voltage"));
    Serial.println(F("7: piezo beeper test"));
    Serial.println(F("8: display sidereal time in endless loop"));
    choice = getInt(F("Choice:"));
    switch (choice)
    {
    case 0:
      sleep();
      break;

    case 1:
      setLng();
      break;

    case 2:
      setRtc();
      break;

    case 3:
      setUTC();
      break;

    case 4:
      setAgeRegister();
      break;

    case 5:
      rotate();
      break;

    case 6:
      showVcc();
      break;

    case 7:
      beeptest();
      break;

    case 8:
      return; // fall into loop
      break;
    }
    delay(1);
  }
}

void loop()
{
  static time_t prev_t = 0;
  power_on();
  setSyncProvider(RTC.get); // Set provider to RTC
  time_t t = now();
  if (t != prev_t)
  {
    // t = 1546300800; // for testing purposes, 2019-01-01 00:00:00
    // t = 1861920000; // for testing purposes, 2029-01-01 00:00:00

    Serial.print("UTC ");
    Serial.print(year(t));
    Serial.print('-');
    print2d(month(t));
    Serial.print('-');
    print2d(day(t));
    Serial.print(' ');
    print2d(hour(t));
    Serial.print(':');
    print2d(minute(t));
    Serial.print(':');
    print2d(second(t));

    long lst = getSidereal(t, loc_lng_d) * 3600L;

    Serial.print(", LST ");
    print2d(lst / 3600);
    Serial.print(':');
    print2d((lst / 60) % 60);
    Serial.print(':');
    print2d(lst % 60);

    Serial.print('\r');

    prev_t = t;
  }
  delay(1);
  power_off();

  delay(10);
}
